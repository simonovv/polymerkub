import './App.css';
import Layout from "./Components/Layout/Layout";

function App() {
  return (
    <div className="App">
     <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0'/>
     <Layout />
    </div>
  );
}

export default App;
