import React, { useState } from "react";
import { Route, Switch } from "react-router-dom";
import Menu from "../Menu/Menu";
import Cart from "../Cart/Cart";
import Search from "../Search/Search";
import Landing from "../Landing/Ladning";

const Layout = () => {
    const [ cartState, setCartState ] = useState(false);

    const testStyle1 = {
      'height': '168px',
      'width': '168px',
      'backgroundColor': 'white',
      'borderRadius': '10px',
      'marginBottom': '6px'
    };

  const testStyle2 = {
    'height': '200px',
    'width': '168px',
    'backgroundColor': 'white',
    'borderRadius': '10px',
    'marginBottom': '6px'
  };

    const changeCartState = () => {
      // if (!cartState) {
      //   document.body.style.overflow = "hidden";
      // } else {
      //   document.body.style.overflow = "inherit";
      // }
      setCartState(!cartState);
    };

    const closeCardHandler = () => {
      setCartState(false);
    };

    // const test = (
    //   <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'space-evenly' }}>
    //     { [1, 2, 3, 4].map((_, i) => <div key={i} style={getStyle(i)}></div>) }
    //   </div>
    // );

    const test = (
      <div id="targetElementId" style={{height: '100%', paddingBottom: '100px', flexGrow: 1, overflow: 'auto'}}>
        <div style={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center' }}>
          <div style={{marginRight: '6px'}}>
            { [1, 2, 3, 4].map((_, i) => <div key={i} style={i % 2 === 0 ? testStyle1 : testStyle2}></div>) }
          </div>
          <div>
            { [1, 2, 3, 4].map((_, i) => <div key={i} style={i % 2 === 0 ? testStyle2 : testStyle1}></div>) }
          </div>
        </div>
      </div>
    );

    const shopPage = (Body: JSX.Element) => (
      <React.Fragment>
        <Search />
        { Body }
        <Menu changeCartState={changeCartState}></Menu>
        { cartState ? <Cart closeCart={closeCardHandler}/> : null }
      </React.Fragment>
    );

    return (
        <div style={{display: 'flex', flexDirection: 'column', height: '100%'}}>
            <Switch>
                <Route path="/about" render={ () => shopPage(<div>About</div>) }/>
                <Route path="/categories" render={ () => shopPage(test) }/>
                <Route path="/cart" render={ () => shopPage(<div>Cart</div>) }/>
                <Route path="/" component={Landing}/>
            </Switch>
        </div>
    );
};

export default Layout;