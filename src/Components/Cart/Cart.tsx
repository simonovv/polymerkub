import React, { RefObject, useState } from "react";
import styles from "./Cart.module.scss";

interface IProps {
	closeCart: () => void;
}

const Cart = (props: IProps) => {
	const myRef: RefObject<HTMLDivElement> = React.createRef();
	const [ offset, setOffset ] = useState(0);
	const onTouchMove = (e: React.TouchEvent<HTMLDivElement>) => {
		const touch = e.touches[0] || e.changedTouches[0];
		const elm = myRef.current?.getBoundingClientRect();
		const touchOffset = touch.pageY - (elm?.top || 0);
		if (offset >= 0 && touchOffset <= 0) {
			setOffset(0);
			return;
		}
		if (offset < -200) {
			props.closeCart();
		}
		console.log(offset, touchOffset, offset - touchOffset);
		// console.log(offset - touch.pageY - (elm?.top || 0));
		setOffset(offset - touchOffset);
	};

	const onTouchEnd = (e: React.TouchEvent<HTMLDivElement>) => {
		setOffset(0)
	}

	return (
		<div className={ styles.backDrop }>
			<div className={ styles.cart } style={{ bottom: `${offset}px` }}>
				<div className={styles.closeContainer} onTouchMove={onTouchMove} onTouchEnd={onTouchEnd} ref={myRef}>
					<div className={ styles.close }></div>
					<div className={styles.cartHeader}>
						<div>
							<svg width="37" height="31" viewBox="0 0 37 31" fill="none" xmlns="http://www.w3.org/2000/svg">
								<path d="M10.3448 24.258C11.4861 24.258 12.3829 25.16 12.3829 26.308C12.3829 27.415 11.4861 28.317 10.3448 28.317C9.20346 28.317 8.30671 27.415 8.30671 26.308C8.30671 25.16 9.20346 24.258 10.3448 24.258ZM28.6875 24.258C29.8288 24.258 30.7256 25.16 30.7256 26.308C30.7256 27.415 29.8288 28.317 28.6875 28.317C27.5462 28.317 26.6494 27.415 26.6494 26.308C26.6494 25.16 27.5462 24.258 28.6875 24.258ZM10.3448 22.208C8.1029 22.208 6.26863 24.053 6.26863 26.308C6.26863 28.522 8.1029 30.367 10.3448 30.367C12.5867 30.367 14.4209 28.522 14.4209 26.308C14.4209 24.053 12.5867 22.208 10.3448 22.208ZM28.6875 22.208C26.4456 22.208 24.6114 24.053 24.6114 26.308C24.6114 28.522 26.4456 30.367 28.6875 30.367C30.9294 30.367 32.7637 28.522 32.7637 26.308C32.7637 24.053 30.9294 22.208 28.6875 22.208ZM34.3941 4.78299L31.0109 14.992H11.2008L9.52956 4.78299H34.3941ZM35.8208 2.73299H8.30671C8.02138 2.73299 7.73604 2.85599 7.53224 3.06099C7.32843 3.30699 7.2469 3.59399 7.28767 3.92199L9.32575 16.222C9.40727 16.714 9.85565 17.042 10.3448 17.083H31.7446C32.193 17.042 32.5599 16.796 32.7229 16.345L36.7991 4.04499C36.8806 3.75799 36.8398 3.38899 36.636 3.14299C36.473 2.85599 36.1469 2.73299 35.8208 2.73299ZM4.23055 0.682994H1.17342C0.806572 0.641994 0.480476 0.846994 0.276669 1.17499C0.113625 1.50299 0.113625 1.87199 0.276669 2.19999C0.480476 2.52799 0.806572 2.73299 1.17342 2.73299H3.37455L6.26863 20.322C6.35015 20.814 6.79853 21.142 7.28767 21.183H30.7256C31.0924 21.183 31.4185 20.978 31.6223 20.65C31.7854 20.322 31.7854 19.953 31.6223 19.625C31.4185 19.297 31.0924 19.092 30.7256 19.133H8.14366L5.24959 1.50299C5.16806 1.01099 4.71968 0.682994 4.23055 0.682994Z" fill="white"/>
							</svg>
							<h1>Корзина</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	);
};

export default Cart;