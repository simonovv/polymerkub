import React from 'react';
import ReactFullpage from '@fullpage/react-fullpage';
import "./Landing.scss";
import Logo from "../Search/Logo.png";
import { Link } from 'react-router-dom';

const Landing = () => (
	<ReactFullpage
		scrollingSpeed = {1000}

		render={({ state, fullpageApi }: { state: any, fullpageApi: any }) => {
			return (
				<ReactFullpage.Wrapper>
					<div className="section first">
						<div className="content">
							<header>
								<img src={Logo} alt="Logo"/>
								<h1>POLYMERKUB</h1>
							</header>
							<main>
								<p>
									Компания “ПолимерКуб” с 2017 года
									обеспечивает качественной профессиональной химией
									строительные фирмы, мебельные фабрики, подрядные организации,
									скульпторов и обычных людей
								</p>
								<button><Link to="/categories">Оплатить</Link></button>
							</main>
						</div>
					</div>
					<div className="section">
						<p>Section 2</p>
					</div>
				</ReactFullpage.Wrapper>
			);
		}}
	/>
);

export default Landing;