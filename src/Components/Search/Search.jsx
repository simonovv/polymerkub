import Logo from "./Logo.png";
import SearchIcon from "./Search.svg";
import styles from "./Search.module.scss"

export const Search = () => (
    <nav className={styles.search}>
        <img src={Logo} alt="Logo"/>
        <img src={SearchIcon} alt="Search"/>
        <input placeholder="Поиск"/>
    </nav>
);

export default Search;