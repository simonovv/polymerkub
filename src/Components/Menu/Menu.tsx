import React, { ComponentProps, RefObject } from 'react';
import { Link } from "react-router-dom";
import styles from './Menu.module.scss';
import MenuIcon from "./Menu.svg";
import CartIcon from "./Cart.svg";
import CategoriesIcon from "./Categories.svg";
import AboutIcon from "./About.svg";
import HomeIcon from "./Home.svg";

interface IState {
    currentMenuWidth: number;
}

interface IProps {
    changeCartState: () => void;
}

const DEFAULT_MAX_MENU_WIDTH = 350;

class Menu extends React.Component<IProps, IState> {
    private readonly myRef: RefObject<HTMLDivElement>;
    constructor(props: ComponentProps<any>) {
        super(props);
        this.myRef = React.createRef();
        this.state = {
            currentMenuWidth: DEFAULT_MAX_MENU_WIDTH
        }
    }

    public componentDidMount() {
        this.setState({
            currentMenuWidth: this.myRef.current?.clientWidth || DEFAULT_MAX_MENU_WIDTH
        });
    }

    render() {
        const { currentMenuWidth } = this.state;
        const { changeCartState } = this.props;
        return (
          <div className={styles.menuWrapper}>
              <div className={styles.menu} ref={this.myRef}>
                  <img src={MenuIcon} alt="Menu"/>
                  <div className={`${styles.menuItemsWrapper} ${styles.leftSide}`} style={{width: currentMenuWidth / 2}}>
                      <Link className={styles.menuItem} style={{marginLeft: currentMenuWidth / 8}} to="/">
                          <img src={HomeIcon} alt="Home"/>
                          <label>Главная</label>
                      </Link>
                  </div>
                  <div className={`${styles.menuItem} ${styles.cart}`} onClick={changeCartState}>
                      <img src={CartIcon} alt="Cart"/>
                      <span>Корзина</span>
                  </div>
                  <div className={`${styles.menuItemsWrapper} ${styles.rightSide}`} style={{width: currentMenuWidth / 2}}>
                      <Link className={styles.menuItem} to="/categories">
                          <img src={CategoriesIcon} alt="Categories"/>
                          <label>Категории</label>
                      </Link>
                      <Link className={styles.menuItem} style={{marginRight: currentMenuWidth / 25}} to="/about">
                          <img src={AboutIcon} alt="About"/>
                          <label>О нас</label>
                      </Link>
                  </div>
              </div>
          </div>
        );
    }
}

export default Menu;